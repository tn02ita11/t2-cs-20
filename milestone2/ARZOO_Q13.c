#include <stdio.h> 
void FibonacciNumbers(int n) 
{ 
    int a = 0, b = 1, c,i; 
  
    if (n < 1) 
        return; 
  
    for (i = 1; i <= n; i++) 
    { 
        printf("%d ", b); 
        c = a + b; 
        a = b; 
        b = c; 
    } 
}  
void main() 
{ clrscr();
    FibonacciNumbers(9); 
    getch();
} 