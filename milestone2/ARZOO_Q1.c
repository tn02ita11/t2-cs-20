#include<stdio.h> 
int power(int x,int n) 
{ 
    if (n == 0) 
        return 1; 
    else if (n%2 == 0) 
        return power(x, n/2)*power(x, n/2); 
    else
        return x*power(x, n/2)*power(x, n/2); 
} 
  void main() 
{ 
    int x = 3, n = 7; 
	clrscr();
    printf("Answer is %d", power(x, n)); 
	getch();
} 
