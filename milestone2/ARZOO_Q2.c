#include <stdio.h> 
int gcd(int x, int y) 
{ 
    if (y == 0) 
        return x; 
    return gcd(y, x % y);  
} 
void main() 
{ 
    int x = 8, y = 12; 
	clrscr();
    printf("gcd of %d and %d is %d ", x, y, gcd(x, y)); 
    getch();
} 