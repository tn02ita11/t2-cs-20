#include<stdio.h> 
  
int f(int n) 
{ 
    if (n <= 1) 
        return 1; 
    return n*f(n-1); 
} 
  
int nPr(int n, int r) 
{ 
    return f(n)/f(n-r); 
} 
  
void  main() 
{ 
    int n, r; 
	clrscr();
    printf("Enter n: "); 
    scanf("%d", &n); 
  
    printf("Enter r: "); 
    scanf("%d", &r); 
  
    printf("%dP%d is %d", n, r, nPr(n, r)); 
  getch();
}