#include<stdio.h>
int str_find_substr(char[], char[]); 
void main() {
   int location;
   char str[100];
   char substr[100];
   printf("Enter the string and the SUBstring to be searched:");
   scanf("%s",str);
   scanf("%s",substr);
   location = str_find_substr(str, substr);
   if (location == -1)
      printf("\t-1");
   else
      printf("\nFound at location %d", location + 1);
	  getch();
}
int str_find_substr(char str[], char substr[]) {
   int i, j, first;
   i = 0, j = 0;
   while (str[i] != '\0') {
      while (str[i] != substr[0] && str[i] != '\0')
         i++;
      if (str[i] == '\0')
         return (-1);
      first = i;
      while (str[i] == substr[j] && str[i] != '\0' && substr[j] != '\0') {
         i++;
         j++;
      }
      if (substr[j] == '\0')
         return (first);
      if (str[i] == '\0')
         return (-1);
      i = first + 1;
      j = 0;
   }
}