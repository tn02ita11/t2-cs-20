#include<stdio.h>
 
int str_find_substr(char[], char[]);
 
int main() {
   int location;
 
   char str[200];
   char substr[200];
   printf("Enter the main string and the string to be searched:");
   scanf("%s",str);
   scanf("%s",substr);
 
   location = str_find_substr(str, substr);
 
   if (location == -1)
      printf("\n-1");
   else
      printf("\nFound at location %d", location + 1);
 
   return (0);
}
 
int str_find_substr(char str[], char substr[]) {
   int i, j, first;
   i = 0, j = 0;
 
   while (str[i] != '\0') {
 
      while (str[i] != substr[0] && str[i] != '\0')
         i++;
 
      if (str[i] == '\0')
         return (-1);
 
      first = i;
 
      while (str[i] == substr[j] && str[i] != '\0' && substr[j] != '\0') {
         i++;
         j++;
      }
 
      if (substr[j] == '\0')
         return (first);
      if (str[i] == '\0')
         return (-1);
 
      i = first + 1;
      j = 0;
   }
}